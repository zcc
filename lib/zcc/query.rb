module ZCC

  class Query
    attr_reader :term, :zservers, :type, :zsearch
    
    def initialize(term, zservers)
      self.term = term #=> string of original query post any filtering
      #puts "term: " + self.term
      @zservers = []
      @zservers << zservers #=>array of Zserver objects
      self.zservers.compact!
      self.zservers.flatten!
      #puts self.inspect
      #puts self.zservers[0].to_s
    end
    
    def term=(term)
      if term =~ /\d+-\d+/
        @term = lccn_conversion(term)
        @type = 'lccn'
        #puts "converted lccn to #{@term}"
        @zsearch = "@attr 1=9 #{@term}"
      elsif term =~ /\d+[X||\d]/ and !(term.match( /[a-wyz]/i ))
        #puts "Searching for ISBN: #{term}"
        @term = term
        @type = 'isbn'
        @zsearch = "@attr 1=7 #{@term}"
      else ( term[/[a-z]/i] ) #-- This check for a string could be better!
        #puts "searching for title:#{term}"
        if term.match(/ :au /)
          term = term.split(" :au ")
          puts "Author and title then"
          @term = term[0]
          @zsearch = "@and @attr 1=4 \"#{@term}\" @attr 1=1 \"#{term[1]} \"" #was: "@attr 1=4 \"'#{@term}'\""
        else
          @term = term
          @zsearch = "@attr 1=4 \"#{@term}\"" #was: "@attr 1=4 \"'#{@term}'\""
        end
        @type = 'title'
        
        #puts self.term
        #puts self.type
        #puts self.zsearch
        
      end
    end
    
    #This is the main search logic of the whole shebang. Aliased as 'search'.
    #The method on a query object and is passed the number of records from each host to present to the user. Default number of records is 10.
    #Still need to work on a way to get options in so this might change.
    #Currently only MARC21 is supported.
    def zoom(show = 10)
      result_set = ResultSet.new(self)
      #puts zservers.inspect
      search_threads=[]
      puts zservers.size.to_s + " z-servers in group " + zservers[0].group.to_s
      z = 0
      self.zservers.each do |server|
          z += 1
          #zservers are made up of...
          #puts server.to_s
        search_threads << Thread.new(server, z) do |myserver, myz|
          begin
            conn = ZOOM::Connection.new
            conn.connect(myserver.host, server.port) #do |conn 
            conn.set_option('charset', 'UTF-8')
            conn.preferred_record_syntax = 'MARC21'
            conn.database_name = myserver.database
            puts "#{myz} Searching:          #{myserver.to_s} | #{self.zsearch}"
            rset = conn.search(self.zsearch)
            say(myz.to_s.headline + " Finished searching: #{myserver.to_s} | rset.size: " + "#{rset.size}".red.bold)
            rset_recs = rset[0, show]
            #puts "rset_recs in query.search: " 
            #puts rset_recs
            i = 0
            rset_recs.each do |rec|
              #puts myserver.to_s
              #puts rec
              #puts
              marc_record = ZCC.convert_char(rec)
              #puts "gets past character conversion"
              #puts "-------------\n"
              #puts marc_record
              #puts "---------------\n"
              zcc_record = Record.new(marc_record, myserver)
              result_set.ingest(zcc_record)
              puts "#{myz} record #{i} from       #{myserver}..."
              i += 1
            end
          rescue Exception => e
            zerror_log("dead thread: " + myserver.to_s + " | " + e)
            puts "\a#{myz}!!!!!!!! Thread died #{myserver} !!!!!"
          end
          #puts "end: #{Thread.list}"
          #puts "Results processed from:  #{myserver.to_s}"
        end
        
      end
      search_threads.each{|thread| thread.join}
      return result_set
    end
    
    alias search zoom

    #FIXME probably should move this to a different class that contants more cli stuff
    def zerror_log error
      File.open("#{ROOT}/zerror_log", "a+") do |f|
        f.write error + "\n"
      end
    end
    
  end
  
  
  def convert_char rsetrec
    rec = MARC::Record.new_from_marc(rsetrec.raw)
    #puts "initial rec" + rec.to_s
    ldr9 = rec.leader[9, 1]
    return_rec = ''
    #puts "gets to creating dummy record."
    if ldr9 == ' '
      #return_rec = MARC::Record.new_from_marc(rsetrec.raw('MARC-8', 'UTF-8')) #This does NOT work
      return_rec = MARC::XMLReader.new(StringIO.new(rsetrec.xml('MARC-8', 'UTF-8'))).to_a
      return_rec = return_rec[0]
      return_rec.leader[9,1] = 'a'
      #puts "return_rec" + return_rec.to_s
    elsif ldr9 == 'a'
      #puts "already unicode"
      return_rec = rec
    else
      raise "Invalid value in leader 9 for MARC21"
    end
    return_rec
  end
  
  def lccn_conversion lccn
    split_lccn = lccn.split('-')
    year_len = split_lccn[0].length
    serial_len = split_lccn[1].length
    start_length = year_len + serial_len
    if year_len == 2
      final_lccn = lccn.gsub('-', "#{'0' * (8 - start_length)}")
    elsif year_len == 4
      final_lccn = lccn.gsub('-', "#{'0' * (10 - start_length)}")
    end
    final_lccn
  end
  
    #++ compare() is a method to print out a comparison of two MARC records tag by tag (not by subfields).
  # A match between lines is denoted with a 'm'. If there are differences between the records,
  # the object that recieves the compare call is denoted with a '+' and the object passed in 
  # parens is denoted with '-'. 
  def compare_marc(orig, rec2)
    puts "+ = #{orig.zserver.to_s}".bold
    puts "- = #{rec2.zserver.to_s}".bold
    orig = orig.marc
    rec2 = rec2.marc
    
    for ft in ('000'..'999')
      fields_original = orig.find_all {|f| f.tag == ft}
      fields_record2 = rec2.find_all {|f| f.tag == ft}
      fields_orig = []
      fields_rec2 = []

      fields_original.each {|f| fields_orig << f.to_s}
      fields_record2.each {|f| fields_rec2 << f.to_s}

      matches = fields_orig & fields_rec2
      matches.each { |f| puts "m".bold + " #{f}" } if matches			

      fields_orig -= matches
      fields_orig.each {|f| puts "+".bold + " #{f}"} if fields_orig

      fields_rec2 -= matches
      fields_rec2.each {|f| puts "-".bold + " #{f}"} if fields_rec2
    end
  end
  
  def blank_field_prompt(field, subfield)
    #puts "subfield: #{subfield}"
    value = ask("\nYour MARC record does not contain #{field}#{subfield}.\nEnter the intended value for #{field}#{subfield} or hit ENTER to leave blank\n#{field}#{subfield} > ")
    value
  end
  
end
