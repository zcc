module ZCC  
  class ResultSet
    include Enumerable
    
    attr_accessor :query, :sort_by, :index_start, :index_pos, :records, :rank
    
    #All values of initialize are optional, though you'll want to supply it with a query object if you intend on getting records into your set through a z39.50 search. index_start and index_end will be used for the TUI display.
    def initialize(query_object=nil, sort_by='title', index_start=0, index_pos=4 ) 
      @query = query_object #query object
      
      @sort_by = sort_by
      @index_start = index_start
      @index_pos = index_pos
      #puts self.sort_by
      #puts self.index_start
      @records = []
      #puts self.records.class
    end
    
    #Pretty prints the result set object. Nests pretty printed record objects within.
    def to_s
      full_string =  "-------RESULT SET--------------\n" + "Result set has #{self.records.size} records."
      self.records.each do |record|
        full_string << record.to_s
      end
      full_string += "-------RESULT SET--------------\n"
    end

    # Method to add records only to a result set.
    def ingest record
      self.records << record
    end
    
    # appends a result set to another.
    # Replaces the query and sort_by instance variables with the new ones.
    
    
    #Removes unselected records from the result set. Uses the selected instance variable to check for true or false.
    def remove_unselected!
      self.records.each_index do |i|
        if self.records[i].nil?
        else
          self.records[i] = nil unless self.records[i].selected
        end
      end
      self.records.flatten!
      self.records.compact!
      self.records.uniq!
    end
    
    #Number of records in the result set
    def size
      self.records.length
    end 
    
    alias length size

    #returns number of ZCC::Records with @selected set to true
    def selected_size
      selected_records = self.find_all{|record| record.selected unless record == nil}
      #puts selected_records.length
      selected_records.length
    end 
    
    alias selected_length selected_size
    
    # This allows for Enumerable mixin.
    def each 
      for record in @records
        yield record
      end
    end
    
    def sort_by_title!
      self.records.compact!
      self.records.flatten!
      self.records = self.records.sort_by{|r| r.marc['245']['a']}
    end

    def sort_by_date!
      self.remove_nil!
      self.records = self.records.sort_by{ |r| r.year_260}         
    end

    def sort_by_subfield! sf
      self.remove_nil!
      field, subfield = sf[0,3], sf[3,10]
      subfield = 'a' if subfield == ''
      nil_subfields = []
      self.records.each_index do |i| 
	      unless self.records[i].marc[field] && self.records[i].marc[field][subfield]
          nil_subfields << self.records[i]
          self.records[i] = nil
	      end
      end
      self.records.compact!
      begin
        self.records = self.records.sort_by{ |r| r.marc[field][subfield]}
      rescue Exception => e
        puts e
        say_help("There was an error.\nYour records have not been sorted.")
      end
      self.records << nil_subfields
      self.remove_nil!
    end

    def sort_by_standard!
      self.remove_nil!
      self.records = self.records.sort_by{|r| r.marc.leader[18]}
      self.records.reverse
    end

    # Very simple relevancy ranking for title searches
    def rank_by_relevance!
      rank = 0
      #raise NotImplementedError
      #unless self.query.type == 'title'
      #  raise "Relevancy ranking only works with titles (and not even them yet)"
      #end
      self.rank
      term = self.query.term
      re_term = Regexp.new("#{term}", true)
      re_term2 = Regexp.new("\^#{term}", true)
      self.records.each do |rec|
        #puts re_term
        #puts rec.marc['245']['a'] =~ re_term
        rec.rank += 5 if rec.marc['245']['a'] =~ re_term
        rec.rank += 10 if rec.marc['245']['a'] =~  re_term2
      end
      self.records = self.records.sort_by{|record| record.rank}
      self.records.reverse!
    end

    def remove_nil!
      self.records.compact!
      self.records.flatten!
    end
    
    def hits_per_source 
    end #=> hash{zurl=>[rec, rec],zurl=>[rec, rec]
    
    def []
      
    
    end # Record at that position
    
    def empty?
      if self.records.size == 0
        return true
      elsif self.records.size > 0
        return false
      end
    end #returns true if the @ZCCRecords array is empty or only nil values
    
    #def find_all
      #self.find_all {|record| record.selected}
    #end

    def <<(result_set)
      #puts record
      self.records << result_set.records
      self.query = result_set.query
      self.sort_by = result_set.sort_by      
      self.records.flatten!
      #puts self.records.inspect
    end

    
  end
end
