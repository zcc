module ZCC
  class Zserver
    attr_reader :host, :port, :database
    attr_accessor :group, :explain, :options
     
    def initialize(host, port, database, group=0, options={'charset'=>'UTF-8'}, *args)
      @host = host
      @port = port
      @database = database
      @group = group
      @options = options
      @user, @password = args
    end
    
    #create a REXML object representation of the explain document
    #def explain=(xml) 
      
    #end
    
    #is this how you create a singleton?
    #def explain.something
      
    #end
      
    
    #other explain information
    def get_explain #uses irspy? 
      
    end 
    
    def to_s 
      "#{self.host}:#{self.port}/#{self.database}"
    end
    
    
    
  end
end
