module ZCC
  HighLine.track_eof = false
  def zcc_select_good_marc(results, take_how_many)
    #puts self[0]
    unless results.is_a? ZCC::ResultSet
      raise ArgumentError, "This Array doesn't have a MARC::Record!" 
    end
    rec_copy = results.records
    recs_length = rec_copy.length
    index_range = (0..recs_length - 1)
    clear = %x{clear}
    
    # Help statements:
    help_help = <<-EOF
      HELP
      For help on a particular topic:
        help [topic]
      topics include:
        #   view a record
        p   pick a record
        u   unselect a record
        r   remove a record
        
        s   sort
        
        c   compare two records
        l   lint a record
        
        f   forwards
        b   backwards
        n   next        
        d   done searching
        q   quit
      EOF
    
    help_num = "View a record by typing in the index number\n> 16." 
    help_p = "Pick the record at that index position into the result set to work on and save.\n>  p3"
    help_u = "Unselect a record.\nIf you have chosen to pick a record and no longer want it,\nyou can unselect it.\n> u2"
    help_r = "Remove the record from the result set. This is a way to narrow the result set.\n> r4\nRemoves record 4 from the result set and unclutter the screen some.\n
    Alternately, just enter 'r' and you will have the chance to enter a range to remove."
    
    help_s = <<-E_O_F
      Sort.
      Hit 's' to get a menu of sort possibilities. 
      Possible sorts include: title, date, subfield, content standard.
      sort by title:   s -> t
      sort by date:    s -> d
      sort by content: s -> c
      Paths to sort by subfield:
      sort -> s 260a
      sort -> subfield -> 260a
      sort -> subfield -> 260    which equals 260a
      s    -> s 260a
      s    -> s        -> 260a
      Sorting reindexes the result set and removes nil values.
      E_O_F

    help_c = "Compare two records.\nCompares the records line by line.\nLines with an 'm' match each other.\nLines with a plus sign are in the first record but not the second. Lines with a minus sign '-' are from the second record but not the first.\nEven the difference of a period matters here.\n> c [ENTER] 4-2"
    help_l = "Lint the record.\nCheck for errors such as wrong indicator values or poor punctuation.\n> l5"

    help_f = "Forwards through the result set.\nThe number of results shown per page is configured in zcc.yaml."
    help_b = "backwards through the result set.\nThe number of results shown per page is configured in zcc.yaml."
    help_n = "Next Z39.50 server/group of zservers.\nIf there are no more zservers to search, you are presented with you combined picks or a new search prompt.\n>  n"
    help_d = "Done selecting records.\nIf at least one record has been selected you continue on to final selection.\nIf no records have been selected you are presented with a search prompt again."
    help_d = "Quit the program."
    
    #the following are not implemented
    help_all = "Select all the records in the result set to work on and save."
    help_none = "Select none of the records from the final set."
    
    not_implemented = "This feature is not yet implemented."

    #take_how_many = 'multi' #'one' # 'multi'

    

    loop do
      
      choose do |menu|
        print $clear_code
        
        menu.layout = :one_line
        menu.readline = true
        menu.shell  = true
        menu.prompt = "Enter "
        menu.select_by = :name
        menu.update_responses
        menu.help("help", help_help)
        
        rec_copy.each_index do |index|
          showings = results.index_start + results.index_pos
          #puts showings
          if index >= results.index_start && index <= showings
          ZCC.display_menu(rec_copy, index)
          end
          #puts "\n\n"
        end

        #convenience to show whole record in the case that there's only one
        if recs_length == 1
          say("#{ZCC.zcc_marc_str_bold(rec_copy[0].to_s, 'record')}")
        end  
        
        puts "Highest position: " + (results.size - 1).to_s
        say("\aYou are currently in the winnowing stage.\nWinnow your choices with 'unselect' or 'remove'.\nEnter 'done' if you want to process all selected records\nor if no records are selected you will be returned to a search.\n".bold) if take_how_many == 'winnow'

        
        menu.hidden("help", help_help) do |cmd, d| 
          if d == ''
            say_help(help_help)
          elsif d == '#'
            say_help(help_num)
          elsif d == 'p'
            say_help(help_p)
            elsif d == 's'
            say_help(help_s)
            elsif d == 'r'
            say_help(help_r)
            elsif d == 'c'
            say_help(help_c)
            elsif d == 'l'
            say_help(help_l)
            elsif d == 'f'
            say_help(help_f)
            elsif d == 'b'
            say_help(help_b)
            elsif d == 'n'
            say_help(help_n)
            elsif d == 'u'
            say_help(help_u)
            elsif d == 'd'
            say_help(help_d)
          end
        end
        
        # # => view
	      menu.choice('#'.intern, help_num) do |command, details|
          say_help(help_num)
        end
        for x in index_range
	        menu.hidden("#{x}".intern, help_num) do |cmd, details|
            if rec_copy[cmd.to_s.to_i] == nil
              say("You removed that record!")
            else
              print "\a"
              say("#{ZCC.zcc_marc_str_bold(rec_copy[cmd.to_s.to_i].to_s, 'record')}")
            end
            ask("Hit ENTER to continue...".headlinez)
          end
        end

        # pick
	      menu.choice("p#".intern, help_p) { |cmd, d|  say_help(help_p) }
        menu.hidden(:p, help_p){|cmd, d| say_help(help_p)}
	      menu.hidden(:pick, help_p) { |cmd, d|  say_help(help_p) }
        for x in index_range
	        menu.hidden("p#{x}") do |cmd, d|
            num_picked = cmd[1,99]
	          rec_copy[num_picked.to_i].selected = true if rec_copy[num_picked.to_i] != nil
            if take_how_many == 'one'
              return results
            end
          end
        end

        #unselect
        menu.choice("u#".intern, help_u) { |cmd, d|  say_help(help_u) }
        menu.hidden(:u, help_u) { |cmd, d|  say_help(help_u) }
	      menu.hidden(:unselect, help_u) { |cmd, d|  say_help(help_u) }
        for x in index_range
	        menu.hidden("u#{x}", help_u) do |cmd, d|
            num_picked = cmd[1,99]
            if rec_copy[num_picked.to_i] != nil
              rec_copy[num_picked.to_i].selected = false
              #return
            end
          end
        end
        
        # remove
        #menu.hidden("r#".intern, help_r) { |cmd, d|  say_help(help_r) }
        menu.hidden(:remove, help_r) { |cmd, d|  say_help(help_r) }
        menu.choice(:r, help_r) do |cmd, d|  
          say(help_r)
          range = ask("Enter range to remove like '2-5' remove records 2, 3, 4 and 5.".boldz)
          range_a = range.split('-').collect{|i| i.to_i}
          if range_a[1]
            for r in range_a[0]..range_a[1]
              rec_copy[r] = nil if rec_copy[r]
              #return
            end
          else
            rec_copy[range_a[0]] = nil
            #return
          end
        end
	      menu.hidden(:remove, help_r) { |cmd, d|  say_help(help_r) }
        
        for x in index_range
	        menu.hidden("r#{x}", help_r) do |cmd, d|
            num_picked = cmd[1,99]
            rec_copy[num_picked.to_i] = nil
            #return
          end
        end
        
        # sort
        menu.hidden(:sort, help_s){|cmd, d| say(help_s)}
	      menu.choice(:s, help_s) do |command, details|
          say(help_s)
          choose do |sort_menu|
            
            sort_menu.layout = :one_line
            sort_menu.readline = true
            sort_menu.shell  = true
            sort_menu.prompt = "Enter "
            sort_menu.select_by = :name
            
            sort_menu.choice(:title, help_s){|cmd, d| results.sort_by_title!}
            sort_menu.choice(:date, help_s){|cmd, d| results.sort_by_date!}
            sort_menu.choice(:content, help_s){|cmd, d| results.sort_by_standard!}
            sort_menu.choice(:subfield, help_s){|cmd, d|
              puts "|" + d + "|"
              if d == ''
                field_subfield = ask("Enter field and subfield like so: 245c > ")
                results.sort_by_subfield!(field_subfield)                
              else
                results.sort_by_subfield!(d)
              end
            }
          end
          #sort_menu(rec_copy)
        end
	      
        # compare
        menu.hidden(:compare, help_c){|c,d| say_help(help_c)}
        menu.choice(:c, help_c) do |c,d| 
          say(help_c)
          comparitors = ask("Enter the two to compare. 1-2 compares record 1 to record 2.".boldz)
          compare_nums = comparitors.split('-').collect{|i| i.to_i}
          if rec_copy[compare_nums[0].to_i] == nil || rec_copy[compare_nums[1].to_i] == nil
              say_help("One of the records has been removed!")
            else
              say("comparison:".headline)
              compare_marc(rec_copy[compare_nums[0].to_i], rec_copy[compare_nums[1].to_i])
              ask("Hit ENTER to continue...".headlinez)            
              next
            end
        end
        #this always gave an Ambiguous choice error in many cases for instance c1-2 would throw the Highline error. Would like to get this to work as originally intended.
	      #menu.choice('c#-#', help_c) do |command, details|
        #  say_help(help_c)
        #end
        # comparison = []
        # for x in (0..recs_length-1)
          # for y in (0..recs_length-1)
            # unless x == y
              # comparison << 'c' + x.to_s + '-' + y.to_s
            # end
          # end
        # end
        # comparison.each do |compare|
          # menu.hidden(compare, help_c) do |cmd, details|
            # cmd = cmd[1,99]
            # compare_nums = cmd.split('-')
            # if rec_copy[compare_nums[0].to_i] == nil || rec_copy[compare_nums[1].to_i] == nil
              # say_help("One of the records has been removed!")
            # else
              # say("comparison:".headline)
              # compare_marc(rec_copy[compare_nums[0].to_i], rec_copy[compare_nums[1].to_i])
              # ask("Hit ENTER to continue...".headlinez)            
              # next
            # end
          # end
        # end

        # lint
        menu.hidden(:l, help_l){|c,d| say_help(help_l)}
	      menu.choice('l#', help_l) do |cmd, d|
          say_help(help_l)
          #ask("Which record do you want to lint? ")
        end
	      menu.hidden(:lint) {|cmd, d| say(help_l)}
        for x in (0..recs_length - 1)
	        menu.hidden("l#{x}") do |cmd, d| 
            if rec_copy[cmd[1,99].to_i] != nil
              rec_copy[cmd[1,99].to_i].linter
              ask("Hit ENTER to continue...".headlinez)
            end
          end
        end

        # forwards
        menu.choice(:f, help_f) do |cmd, d| 
          unless results.index_start + results.index_pos + 1 >= results.size
            results.index_start += results.index_pos + 1
	        else
            puts "\a"
          end
        end
  
        # backwards
        menu.choice(:b, help_b) do |cmd, d| 
          unless results.index_start == 0
            results.index_start -= (results.index_pos + 1)
          else
            puts "\a"
          end
        end

        # next
	      if take_how_many == 'multi'
          menu.choice(:n, help_n) do |command, details|
            return 'next'
          end
        end
        
        # done => selected as many 
	      unless take_how_many == 'one'
          menu.choice(:d, help_d) do |cmd, d|
            return 'done'
          end
        end

        # quit
	      menu.choice(:quit, "Exit program.") { |cmd, d| exit}
        
        # none -- only for final record taking. not currently used
	      if take_how_many == 'one'    
	        menu.choice(:none, help_none) do |cmd, d| 
            say("Since you cannot decide on a good record, please refer the book to a cataloger.".headline)
            return "none"	
          end
        end        
        
      end
    end
  end

  def display_menu(rec_copy, index)    
    field_width = $term_width - 8
    if rec_copy[index].nil?
      say(index.to_s + " Removed from set.")      
    else
      if rec_copy[index].selected
        say(index.to_s.headlinez)
      else
        say(index.to_s.red.boldz)
      end
      
      # print rank
      if rec_copy[index].rank >= 10
        print " **".boldz
      elsif  rec_copy[index].rank >= 5
        print "  *".boldz
      else 
        #nothing
      end
      say("\t\t" + rec_copy[index].zserver.to_s)
      #['245', '260', '300'].each do |field|
      FIELDS_TO_SHOW.each do |field|
        field = field.to_s
        string = rec_copy[index].marc[field].to_s
        string.rstrip!
        string.lstrip!
        begin
          if string.length < field_width
            say("\t" + ZCC.zcc_marc_str_bold(string, field))
          else
            better_string = ZCC.wrap_field(string, field_width)
            say("\t" + ZCC.zcc_marc_str_bold(better_string, field))
          end
        rescue
          #The dp here stands for display problem.
          puts "  dp\t#{rec_copy[index].marc[field].to_s}"
          next
        end
      end
      puts "\n"
    end        
  end
  
  #currently this goes word by word. how difficult to go field by subfield?
  def wrap_field(s, width)
    lines = []
    line = ""
    smaller_width = width - 7
    s.split(/\s+/).each do |word|
	    if (line.size + word.size) >= (width - 3)
        lines << line 
        line = word
        width = smaller_width
	    elsif line.empty?
        line = word
	    else
        line << " " << word
      end
	  end
	  lines << line if line
    return lines.join("\n\t\t")
  end
  
  
  def zcc_marc_str_bold(string, field)
    #puts field
    #string.gsub!("'", "\'")
    #unless field == 'record'
    #  string.gsub!('"', '\"')
    #end
    #string.gsub!("(", "\(")
    #string.gsub!(")", "\)")
    if field == '245'
      string.gsub!(/(\$a\s)(.*?)(\$.|$)/, "\\1" + "\\2".blue + "\\3")
    elsif field == '260'
      #puts 'gets here'
      string.gsub!(/(\$c\s)([\[0-9A-Za-z\]]*)(.|$)/,  "\\1" + "\\2".blue + "\\3")
      #string.gsub!(/(\$c)(.*)(\$\s)/,  "\\1<%= color(\"\\2\", :field_hilite) %>\\3")
      #string.gsub!(/(\$c)(.*)(\.|$)/,  "\\1<%= color(\"\\2\", :field_hilite) %>\\3")
    elsif field == '300'
      string.gsub!(/(\$a)(.*?)(\$|$)/, "\\1" + "\\2".blue + "\\3")
    elsif field == 'record'
      string.sub!(/(LEADER.{19})(.{1})/, "\\1" + "\\2".bold.blue) #colorizes the value of the standard (AACR2, ISBD, or none)
      string.sub!(/(LEADER.{10})(.{1})/, "\\1" + "\\2".bold.blue) #colorizes the
    end
    string.gsub!( /(\$.)/, "\\1".bold )
    string.gsub!( /^(\d\d\d)\s/, "\\1 ".bold)
    string
  end
  
  def not_valid_value
    print $clear_code
    say("\aThat's not a valid value. Try again.".headline)
    sleep 1
    print "\a"
  end


  def say_help h
    say(h)
    ask("Hit enter to continue.".boldz)
   
  end
  
  def sort_menu records
    loop do
      choose do |menu|
        menu.layout = :menu_only
        menu.readline = true
        menu.shell  = true
        #menu.prompt = "> "
        menu.select_by = :index_or_name
        menu.choice(:author){|cmd, d| say("not implemented yet") }
        menu.choice(:date){|cmd, d| say("not implemented yet")}
        menu.choice(:subfield, "in the form of 245a"){|cmd, d| say("not implemented yet")}
        menu.choice(:relevancy){|cmd, d| say("not implemented yet")}
      end
    end
  end

  
  
  
end


# Override HighLine's own defaults so that our large menu options do not display.
# This needs work to have better help for these error messages.

 class Highline
   class Menu
     def update_responses(  )
       append_default unless default.nil?
       @responses = { 
         :ambiguous_completion =>    "Ambiguous choice.  ",                     
         :ask_on_error         =>    "?  ",                     
         :invalid_type         =>    "You must enter a valid option.",                     :no_completion        =>    "You must pick a valid option.",                     :not_in_range         =>    "You must input a valid option." ,                     
         :not_valid            =>    "You must have a valid option." 
       }.merge(@responses)
     end    
   end
 end
 

