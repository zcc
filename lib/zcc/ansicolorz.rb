# HighLine has a lot of Erb junk in its colorizing that makes the text difficult to read.
# Term::ANSIColor is much simpler. Other colorising
class String
  include Term::ANSIColor
end

module Term
  module ANSIColor
    attributez = [:clear, :reset,   :bold,  :dark,   :italic,  :underline,  :underscore, :blink,   :rapid_blink,   :negative,  :concealed, :strikethrough,   :black,  :red,  :green, :yellow,  :blue,  :magenta,   :cyan,   :white,   :on_black,   :on_red,   :on_green, :on_yellow,  :on_blue,    :on_magenta,  :on_cyan, :on_white ]
    attributez.each do |c|
      eval %Q{
        def #{c}z
          #{c} + " "
        end
      }
    end
    def headline
      white.bold.on_black
    end
    def headlinez
      headline + " "
    end
  
    
  end
end
