module Zcc
end

require 'zcc/version'
#require 'zcc/marcadditions'
require 'zcc/cli_display'
#require 'zcc/subfieldeditor'
require 'zcc/query'
require 'zcc/resultset'
require 'zcc/zserver'
require 'zcc/record'
require 'zcc/ansicolorz'
