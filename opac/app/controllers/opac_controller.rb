class OpacController < ApplicationController

  #search & record views
  
  def record
    @record = params[:r]
    @back = params[:term]
    @start = params[:start]
    @per_page = params[:per_page]
    conn = ZOOM::Connection.new
    conn.connect("localhost", 9999)
    conn.set_option('charset', 'UTF-8')
    conn.preferred_record_syntax = 'MARC21'
    conn.database_name = "zcc"
    search_term = "@attr 1=1016 \"#{@record}\""
    rset = conn.search(search_term)
    #puts "rset.size: #{rset.size}"
    
    record = MARC::Record.new_from_marc(rset[0].raw)
    if record['020'] && record['020']['a']
      isbn = record['020']['a']
      isbn_a = isbn.split(/\s/)
      puts isbn_a
      @isbn = isbn_a[0]
    end
    @isbd = isbd record
    
    marc_record = record.to_s
    marc_record.gsub!("LEADER", "<br>LEADER")
    @records_string = marc_record.gsub("\n", "<br>")
  end

  def isbd r
    string = "<span id=\"title\">"
    string += r['245']['a'].to_s + r['245']['b'].to_s + r['245']['c'].to_s + "</span>" 
    string += "<br /><br /> Published: " + r['260']['a'].to_s + " " + r['260']['b'].to_s + " " + r['260']['c'].to_s 
    string += "<br /><br /> Edition: " + r['250']['a'].to_s if r['250']
    string += "<br /><br />Physical Description: " + r['300']['a'].to_s + r['300']['b'].to_s + " " + r['300']['c'].to_s
    string += "<br><br>ISBN: " + r['020']['a'].to_s if r['020']
    fields = r.find_all{|f| f.tag == "650"}
    string += "<br /><br />Subjects: <br/>" if fields.size > 0
    fields.each do |f|
      subject = []
      f.each{|sf| subject << sf.value}
      string += subject.join("--")
      string << "<br />"
    end
    string += "<br /><br />" + r['505']['a'] if r['505']
    string 
  end
  
  def search
    start_time = Time.now
    p start_time
    params[:start] = params[:term] == session[:search_term] ? params[:start] : 1
    @data = params[:term] ? params[:term] : ""
    session[:search_term] = @data
    @form_value = @data.gsub("\"","&quot;")
    
    @start = params[:start] ? params[:start].to_i : 1
    p @start
    @per_page = params[:per_page] ? params[:per_page].to_i : 20
    #data_a = []
    #data_a = @data.split("\s") unless @data.nil?
    data_a = split_params(@data)
    puts data_a.inspect
    if data_a.size > 0
      #search_term = "@attr 2=102 #{" @and " * (data_a.size - 1)} @attr 1=1016 " + data_a.join(" @attr 1=1016 ")
      search_term = "@attr 2=102 #{" @and " * (data_a.size - 1)} "
      data_a.each{|t| search_term << ' @attr 1=1016 "' + t + '"'}
    else
      search_term = '@attr 1=4 ""'
    end
    puts search_term
    conn = ZOOM::Connection.new
    conn.connect("localhost", 9999)
    conn.set_option('charset', 'UTF-8')
    conn.preferred_record_syntax = 'XML'
    conn.database_name = "zcc"
    
    puts "Searching"
    rset = conn.search(search_term)
    puts rset.size
    @rset_size = rset.size
    unless @rset_size == 0
      rset_recs = rset[@start - 1, @per_page]
      p "gets past rset_recs"
      @total_records = []
      all = ''
      rset_recs.each do |rec|
        #p rec
        begin
          #marc_record = ZCC.convert_char(rec)
          ##@total_records << MARC::Record.new_from_marc(rec.raw)
          all << rec.xml
          #puts @total_records
        rescue
          next
        end
        #@total_records << marc_record
      end
      @total_records = MARC::XMLReader.new(StringIO.new(all))
    end
    #puts @total_records
    #puts "gets here"
    @time_difference = Time.now - start_time
    p Time.now
    puts
  end


  
  def split_params(params)
    # Find all phrases enclosed in quotes and pull
    # them into a flat array of phrases
    phrases = params.scan(/"(.*?)"/).flatten
  
    # Remove those phrases from the original string
    left_over = params.gsub(/"(.*?)"/, "").squeeze(" ").strip
  
    # Break up the remaining keywords on whitespace
    keywords = left_over.split(/ /)
  
    # Return one single array of key phrases
    keywords + phrases
  end
  
end
