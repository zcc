class SearchController < ApplicationController
  def found
    start_time = Time.now
    @data = params[:term] ? params[:term] : "beer"
    @form_value = @data.gsub("\"","&quot;")
    @start = params[:start] ? params[:start].to_i : 0
    @per_page = params[:per_page] ? params[:per_page].to_i : 20
    #data_a = []
    #data_a = @data.split("\s") unless @data.nil?
    data_a = split_params(@data)
    puts data_a.inspect
    if data_a.size > 0
      #search_term = "@attr 2=102 #{" @and " * (data_a.size - 1)} @attr 1=1016 " + data_a.join(" @attr 1=1016 ")
      search_term = "@attr 2=102 #{" @and " * (data_a.size - 1)} "
      data_a.each{|t| search_term << ' @attr 1=1016 "' + t + '"'}
    else
      search_term = '@attr 1=4 ""'
    end
    puts search_term
    conn = ZOOM::Connection.new
    conn.connect("localhost", 9999)
    conn.set_option('charset', 'UTF-8')
    conn.preferred_record_syntax = 'XML'
    conn.database_name = "zcc"
    
    puts "Searching"
    rset = conn.search(search_term)
    @rset_size = rset.size
    
    rset_recs = rset[@start, @per_page]
    
    @total_records = []
    all = ''
    rset_recs.each do |rec|
      #puts rec
      begin
        #marc_record = ZCC.convert_char(rec)
        ##@total_records << MARC::Record.new_from_marc(rec.raw)
        all << rec.xml
        #puts @total_records
      rescue
        next
      end
      #@total_records << marc_record
    end
    @total_records = MARC::XMLReader.new(StringIO.new(all))
    #puts @total_records
    #puts "gets here"
    @time_difference = Time.now - start_time
  end

  def split_params(params)
    # Find all phrases enclosed in quotes and pull
    # them into a flat array of phrases
    phrases = params.scan(/"(.*?)"/).flatten
  
    # Remove those phrases from the original string
    left_over = params.gsub(/"(.*?)"/, "").squeeze(" ").strip
  
    # Break up the remaining keywords on whitespace
    keywords = left_over.split(/ /)
  
    # Return one single array of key phrases
    keywords + phrases
  end
end
