class RecordController < ApplicationController

  def show
    @record = params[:r]
    @back = params[:term]
    @start = params[:start]
    
    conn = ZOOM::Connection.new
    conn.connect("localhost", 9999)
    conn.set_option('charset', 'UTF-8')
    conn.preferred_record_syntax = 'MARC21'
    conn.database_name = "zcc"
    search_term = "@attr 1=1016 \"#{@record}\""
    rset = conn.search(search_term)
    puts "rset.size: #{rset.size}"
    
    record = MARC::Record.new_from_marc(rset[0].raw)
    if record['020'] && record['020']['a']
      isbn = record['020']['a']
      isbn_a = isbn.split(/\s/)
      puts isbn_a
      @isbn = isbn_a[0]
    end
    @isbd = isbd record
    
    marc_record = record.to_s
    marc_record.gsub!("LEADER", "<br>LEADER")
    @records_string = marc_record.gsub("\n", "<br>")
  end

  def isbd r
    string = "<span id=\"title\">"
    string += r['245']['a'].to_s + r['245']['b'].to_s + r['245']['c'].to_s + "</span>" 
    string += "<br /><br /> Published: " + r['260']['a'].to_s + " " + r['260']['b'].to_s + " " + r['260']['c'].to_s 
    string += "<br /><br /> Edition: " + r['250']['a'].to_s if r['250']
    string += "<br /><br />Physical Description: " + r['300']['a'].to_s + r['300']['b'].to_s + " " + r['300']['c'].to_s
    string += "<br><br>ISBN: " + r['020']['a'].to_s if r['020']
    fields = r.find_all{|f| f.tag == "650"}
    string += "<br /><br />Subjects: <br/>" if fields.size > 0
    fields.each do |f|
      subject = []
      f.each{|sf| subject << sf.value}
      string += subject.join("--")
      string << "<br />"
    end
    string += "<br /><br />" + r['505']['a'] if r['505']
    string 
  end
  
end
