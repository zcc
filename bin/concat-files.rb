
newfile = File.new("#{File.expand_path("~")}/concat-file.txt", "w")
File.open("#{File.expand_path("~")}/concat-file.txt", "w") do |f|
  Dir["#{ARGV[0]}/*"].sort.each do |file|
    File.open(file, 'r') do |cvs|
      f.puts cvs.read
    end
  end
end
