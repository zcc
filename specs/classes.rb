#       PSEUDO!!!!!!!!!!!

module ZCC
  class Configuration
    
    
  end
  
    
  

  
  
  class MARC21 < Record
    @marc #MARC::Record obj
    @authorities #array of ZCC::Authority records
    def title end #245a
    def author end #1XX?
    def extent end #260a
    def year_260 end
    def years_fixed end
    def save(format) end #save MARC record to file in proper format (MARC, MARCXML)
  end
  
  #later
  class UNIMARC < Record end
  class XML < Record end
  class MODS < XML end
  

  
  class Authority
    @MARC #MARC::Record representation of the authority record
    @type #PN = personal name, CO = corporate...
    @source #usually LC
    @wcid #a WCID object. make sure to include the full xml in a wcid object if it's not already
  end

end

module ZCC #module methods    
  
  def update_zservers  #updates the zservers.yaml file.
    #reads in the zservers.yaml file
    #finds a match in irspy
    #extracts a Zserver obj for each
  end
end
