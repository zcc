# (C) Jason Ronallo
#July 2007

require 'rubygems'
require "rexml/document"
require "rexml/xpath"
$KCODE = 'u'
require 'jcode'


puts "this will take a while..."
contents = open(ARGV[0], 'r') { |f| f.read }
puts "finished reading in file..."
contents.gsub!("\n", '')
#puts contents
puts "splitting the file up!"
records = contents.split('<?xml version="1.0" encoding="UTF-8"?>')

match = 0
good_matches = 0
really_good_matches = 0
ok_matches = 0
all_lines = []
oks = []
nums = []
records.each do |record|
  next if record == ""
  puts "---------------------------"
  puts "rexml is reading in the contents of a record..."
  rec = REXML::Document.new record
    puts rec.elements["explain/recordInfo"]
    #STDIN.gets
     #puts rec.elements["databaseInfo/title"].text
      if rec.elements["explain/databaseInfo/title"]
        puts "Looking in #{rec.elements["explain/databaseInfo/title"].text} ..."
      else
        puts "Untitled... "
      end
      if rec.elements["explain/recordInfo/recordSyntax[@name='usmarc']"] || rec.elements["explain/recordInfo/recordSyntax[@name='USmarc']"]
        
        #get record syntaxes supported
        syntaxes = []
        rec.elements.each("explain/recordInfo/recordSyntax") do |syn|
          syntaxes << syn.attributes["name"]
        end

        
        if rec.elements["explain/serverInfo[@protocol='Z39.50']"]
          puts "we've got a match!"
          match += 1
          
          #how reliable is the target?
          a = []
          rec.elements.each("explain/irspy:status/irspy:probe") do | elem |
            a << elem.attributes["ok"]
          end
          total = 0
          a.each do |b|
            total += b.to_i
          end
          puts total
          puts a.length
          ok = 0.0
          unless a.length == 0
            ok =  100.0 * (total.to_f / a.length.to_f)
            if ok < 50
              puts "not a reliable site!"
              #STDIN.gets
              next
            end
            puts ok.floor
            oks << ok.floor
          end            
          
          
          #create the yaml
            line =  "- [ " + rec.elements["explain/serverInfo/host"].text + ",  "  
            line << rec.elements["explain/serverInfo/port"].text + ",  "
            line << rec.elements["explain/serverInfo/database"].text	
            if ok > 90
              num = 100 - ok.floor + 10 + rand(90)
              line << ", #{num}] #"
              nums << num
            else
              line << ", #{ok.floor + 100}] #"
            end
                      
            line << rec.elements["explain/databaseInfo/title"].text if rec.elements["explain/databaseInfo/title"] && rec.elements["explain/databaseInfo/title"].text
            line << "  " + ok.floor.to_s + '%' if ok
            line << " | " + syntaxes.join(", ")
            puts line
            #STDIN.gets
            all_lines << line << "\n"
        end
      end
end
  puts all_lines
  puts "matches: #{match}"
  

  File.open("irspy-#{Time.now.strftime("%Y%m%d%H%M%S")}\.yaml", "w") do |f|
    f.write all_lines
  end
  oks.sort!
  puts oks.join(", ")
  puts "------------------------"
  nums.sort!
  puts nums.join(" + ")
  
 # puts "really reliable 98%+: #{really_good_matches}"
  #puts "reliable 95%+:        #{good_matches}"
  #puts "still good 90%+       #{ok_matches}"

